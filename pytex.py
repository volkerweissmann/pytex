#!/usr/bin/python3
#    PyTex, the python latex splitter
#    Copyright (C) 2018 Volker Weißmann . Contact: volker.weissmann@gmx.de

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import shutil
import sys
import re
import argparse

parser = argparse.ArgumentParser(description="PyTex, the python latex splitter")
parser.add_argument("source", help="Path of the source dirctory.")
#parser.add_argument("--pass", action="append", metavar="arg", help="Pass the argument arg to the python script")
#parser.add_argument("--onlysec", help="--onlySec=2,5 would make qasdad ignroe all BEGIN_QASDAD sections with a number that is not equal to 2 or 5")
args = parser.parse_args()

SOURCE_PATH = args.source
if not os.path.isfile(SOURCE_PATH) and not os.path.isdir(SOURCE_PATH):
	print("source does not exist: ", SOURCE_PATH)
	exit(2)
sps = list(os.path.split(SOURCE_PATH))
if sps[-1] == "": #This if is neccessary because the user can run pytex.py dirA/ dirB or qasdad.py dirA dirB
	sps = sps[:-1]
sps[-1] += "_pytex_output"
OUT_PATH=os.path.join(*sps)

try:
	shutil.rmtree(OUT_PATH)
except FileNotFoundError: #TODO er soll nur die exception abfangen die kommt wenn OUT_PATH nicht existiert, aber nicht die kommt wenn shutil nicht installiert is. Außerdem muss shutil in die dependency liste rein kommen
	pass

try:
	os.makedirs(OUT_PATH)
except:
	pass
DATA_PATH = os.path.join(OUT_PATH, "pytex_out")
os.makedirs(DATA_PATH)
extracted = {}
def readFile(sourcepath, latexout):
	source = open(sourcepath, "r").read()
	latexFile = open(latexout, "w")
	num = None
	i = 0
	while i < len(source):
		if source[i:i+3] == "\\py" and source[i+3].isdigit():
			if num is not None:
				print("File: " + sourcepath + "\nLine:" + str(1+source[:i].count("\n"))+"\nA \\py after \\py without a \\tex in between is invalid!")
				exit(3)
			i += len("\\py")
			start = i
			while not source[i].isspace():
				i += 1
			try:
				num = float(source[start:i])
			except:
				print("File: " + sourcepath + "\nLine:" + 1+source[:i].count("\n")+"\n\\py is followed by something else than a float.")
				exit(3)
			print(num)
			exit(1)
			if num in extracted:
				print("File: " + sourcepath + "\nLine:" + 1+source[:i].count("\n")+"\nmultiple sections with the number" + str(num))
				exit(3)
			path = os.path.join(DATA_PATH,str(num) + ".tex")
			extracted[num] = "\nsetTexFile(open(\""+ os.path.relpath(path, start=OUT_PATH).replace("\\","\\\\").replace("\"","\\\"")  +"\",'w'))\n"
			path = os.path.relpath(path,start=os.path.dirname(latexout))
			latexFile.write("\\input{" + path + "}\n")
			while source[i].isspace():
				i += 1
			continue
		elif source[i:i+len("END QASDAD \\fi")] == "END QASDAD \\fi":
			if num is None:
				print("File: " + sourcepath + "\nLine:" + 1+source[:i].count("\n")+"\END QASDAD without BEGIN QASDAD")	
			num = None
			i += len("END QASDAD \\fi")
			continue
		if num is None:
			latexFile.write(source[i])
		else:
			if args.onlysec is None:
				extracted[num] += source[i]
			else:
				seclist = [float(s) for s in args.onlysec.split(",")]
				if num in seclist:
					extracted[num] += source[i]
		i += 1

			
if os.path.isfile(SOURCE_PATH):
	readFile(SOURCE_PATH, os.path.join(OUT_PATH,os.path.basename(SOURCE_PATH)))
elif os.path.isdir(SOURCE_PATH):
	for subdir, dirs, files in os.walk(SOURCE_PATH):
		for file in files:
			sourcepath = os.path.join(subdir, file)
			rel = os.path.relpath(sourcepath,start=SOURCE_PATH)
			pathout = os.path.join(OUT_PATH,rel)
			if(file.endswith(".tex")):
				readFile(sourcepath,pathout)
			else:
				os.makedirs(os.path.dirname(pathout), exist_ok=True)
				os.symlink(os.path.relpath(sourcepath,start=os.path.dirname(pathout)), pathout)
				#shutil.copy(sourcepath,pathout)
else:
	print("the source argument:", SOURCE_PATH, "is neither a file nor a directory") #This case should never trigger cause we check this above
	exit(2)

pythonPath = os.path.join(DATA_PATH, "python.py")
pythonFile = open(pythonPath, "w")
pythonFile.write(
'''def tex(*args):
	for a in args:
		TEX_FILE.write(str(a)) 
''')
for key in sorted(extracted.keys()):
	pythonFile.write(extracted[key])
pythonFile.close()